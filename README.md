# mearo - dynamic button grid menu

Mearo is a GTK application written in common Lisp for linux, which can be
used to create a simple button grid menu.  It is configured, either at
compile time, or at runtime, using s-expressions.

## Compilation and Installation

The `Makefile` can be used to compile and install the application.  It is
developed using `SBCL` and uses `asdf` for the system configuration.

## Help

```
Usage: mearo [OPTIONS]...
Create a simple button grid menu.

With no config file given, the default config file will be used.

Available options:
  -c, --config-file FILE     the configuration file to be used
  -C, --config-string STRING
                             the configuration to be used
  -v, --version              output version information and exit
  -h, --help                 display this help and exit

```

## Configuration

There are multiple different ways to configure `mearo`.  It can either be
configured at compile time, or at runtime.

For the compile time configuration, enable the `*static-conf*` parameter
defined in `static-conf.lisp` and change it as desired.

For the runitme configuration, by default the file `~/.config/mearo/config.lisp-expr`
is used.  It is also possible to specify another file or even a string directly,
using the command line options.

The configuration format is always the same.  The example configuration
file `test-config.lisp-expr` can be found in the repository.

Each list in the configuration is a property list.  The first list contains the
general configuration.  Here, the title of the window, its size and other
properties can be specified.

The second list contains the rows of the grid.  Each row is itself list,
where each element represents a column.  Each element in the grid is a
single button.  The button has an image, a label and an action.  The action
is executed inside a shell and can execute any program.  The rows do not
need to have the same amount of columns.

Each property in the list should be self-explanatory.  The full list of
supported properties can be found in the `*conf-desc*` parameter, which
is specified in the `conf.lisp` file.

## Future development

I would love to remove the GTK dependency and use xlib directly.  There
is no real reason why GTK is used, other than convenience.  There are
currently no plans for further development.

## License

Copyright (c) 2022 Christoph Göttschkes

Licensed under the ISC License.
