
LISP ?= sbcl
SRC != find . -name '*.lisp'

all: mearo

mearo: mearo.asd $(SRC)
	$(LISP) --non-interactive \
	        --eval '(require "asdf")' \
	        --eval '(asdf:load-asd (merge-pathnames "mearo.asd" (uiop:getcwd)))' \
	        --eval '(asdf:load-system :mearo)' \
	        --eval '(asdf:make :mearo)' \
	        --eval '(uiop:quit)'

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f mearo $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/mearo

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/mearo

clean:
	$(RM) mearo

.PHONY: all clean install uninstall
