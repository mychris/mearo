;;;; mearo.asd

#+sb-core-compression
(defmethod asdf:perform ((o asdf:image-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression t))

(defsystem #:mearo
  :author "Christoph Göttschkes"
  :maintainer "Christoph Göttschkes"
  :license "ISC"
  :homepage "https://codeberg.org/mychris/mearo"
  :version (:read-file-form "version.lisp-expr")
  :build-operation "program-op"
  :build-pathname "mearo"
  :entry-point "mearo::plain-main"
  :depends-on (#:cl-cffi-gtk
	       #:cl-ppcre
	       #:unix-opts
	       #:uiop)
  :components ((:module "src"
		:serial t
		:components
		((:file "package")
		 (:file "conf")
		 (:file "static-conf")
		 (:file "view-gtk")
		 (:file "mearo"))))
  :description "")
