;;;; view-gtk.lisp

(in-package #:mearo)

(defclass app-view-gtk ()
  ((main-window :accessor app-view-gtk-main-window
		:initarg :main-window
		:documentation "The GTK main window")
   (main-grid :accessor app-view-gtk-main-grid
	      :initarg :main-grid
	      :documentation "The GTK main grid of the window")
   (button-grids :accessor app-view-gtk-button-grids
		 :initarg :button-grids
		 :documentation "Button grids, one for each row of buttons")
   (button-press-callback :accessor app-view-gtk-button-press-callback
			  :initarg :button-press-callback
			  :documentation "Callback called when a button is pressed")
   (all-buttons :accessor app-view-gtk-all-buttons
		:initarg :buttons
		:documentation "All the buttons in a 2-D array")
   )
  (:default-initargs
   :main-window nil
   :main-grid nil
   ;; assume that there never will be more than 128 rows
   :button-grids (make-array '(128) :initial-element nil)
   :button-press-callback nil
   :buttons (make-array '(128 128) :initial-element nil)
   )
  (:documentation "GTK data of the application"))

(defun make-app-view-gtk (title width height)
  (make-instance 'app-view-gtk
		 :main-window (make-instance 'gtk:gtk-window
					     :type :toplevel
					     :title title
					     :default-width width
					     :default-height height
					     :border-width 10
					     :decorated nil
					     :wmclass-name "mearo"
					     :wmclass-class "mearo"
					     :resizable nil)
		 :main-grid (make-instance 'gtk:gtk-grid
					   :column-spacing 10
					   :row-spacing 20
					   :column-homogeneous t)))

(defmethod app-view-add-button ((view app-view-gtk) row col image text)
  (handler-case 
      (let* ((pixbuf (gdk-pixbuf:gdk-pixbuf-new-from-file image))
	     (image-widget (make-instance 'gtk:gtk-image
					  :pixbuf pixbuf))
	     (button-widget (make-instance 'gtk:gtk-button
					   :image image-widget
					   :tooltip-text text
					   :relief :none
					   :hexpand nil
					   :focus-on-click nil))
	     (label-widget (make-instance 'gtk:gtk-label
					  :label text
					  :use-markup t))
	     (new-button (make-instance 'button-view-gtk
					:widget button-widget
					:label label-widget
					:orig-pixbug pixbuf
					:image image-widget)))
	(when (not (aref (app-view-gtk-button-grids view) row))
	  (setf (aref (app-view-gtk-button-grids view) row)
		(make-instance 'gtk:gtk-grid
			       :column-spacing 10
			       :row-spacing 10
			       :column-homogeneous t)))
	(gtk:gtk-grid-attach (aref (app-view-gtk-button-grids view) row) button-widget col 0 1 1)
	(gtk:gtk-grid-attach (aref (app-view-gtk-button-grids view) row) label-widget col 1 1 1)
	(setf (aref (app-view-gtk-all-buttons view) row col) new-button)
	(gobject:g-signal-connect button-widget "enter-notify-event"
				  (lambda (btn event)
				    (declare (ignore btn event))
				    t))
	(gobject:g-signal-connect button-widget "button-press-event"
				  (lambda (btn event)
				    (declare (ignore btn event))
				    t))
	(gobject:g-signal-connect button-widget "button-release-event"
				  (lambda (btn event)
				    (declare (ignore event))
				    (when (app-view-gtk-button-press-callback view)
				      (loop for row-num from 0 below (array-dimension (app-view-gtk-all-buttons view) 0)
					    do (loop for col-num from 0 below (array-dimension (app-view-gtk-all-buttons view) 1)
						     do (when (and (app-view-button view row-num col-num)
								   (eq btn (button-view-gtk-widget (app-view-button view row-num col-num))))
							  (funcall (app-view-gtk-button-press-callback view) row-num col-num)))))
				    t))
	new-button)
    (glib:g-error (e) ())))

(defmethod app-view-window-background ((view app-view-gtk) color)
  (gtk:gtk-widget-override-background-color (app-view-gtk-main-window view) :normal (gdk:gdk-rgba-parse color)))

(defmethod app-view-window-add-keyboard-handler ((view app-view-gtk) callback)
  (gobject:g-signal-connect (app-view-gtk-main-window view) "key_press_event" callback))

(defmethod app-view-window-add-button-pressed-handler ((view app-view-gtk) callback)
  (setf (app-view-gtk-button-press-callback view) callback))

(defmethod app-view-button ((view app-view-gtk) row col)
  (aref (app-view-gtk-all-buttons view) row col))

(defmethod app-view-show ((view app-view-gtk))
  (setf (gtk:gtk-window-role (app-view-gtk-main-window view)) "mearo")
  (gtk:gtk-container-add (app-view-gtk-main-window view) (app-view-gtk-main-grid view))
  (gobject:g-signal-connect (app-view-gtk-main-window view) "destroy"
			    (lambda (widget)
			      (declare (ignore widget))
			      (gtk:leave-gtk-main)))
  (loop for row-grid across (app-view-gtk-button-grids view)
	for row-num from 0
	do (when row-grid
	     (gtk:gtk-grid-attach (app-view-gtk-main-grid view) row-grid 0 row-num 1 1)))
  (gtk:gtk-widget-show-all (app-view-gtk-main-window view)))

(defun app-view-quit ()
  (gtk:leave-gtk-main))

(defclass button-view-gtk ()
  ((widget :accessor button-view-gtk-widget
	   :initarg :widget
	   :type gtk:gtk-button
	   :documentation "The GTK widget representing this button")
   (label :accessor button-view-gtk-label
	  :initarg :label
	  :type gtk:gtk-label
	  :documentation "The GTK widget representing the label of this button")
   (orig-pixbuf :accessor button-view-gtk-image-pixbuf
		:initarg :orig-pixbug
		:type gdk-pixbuf:gdk-pixbuf
		:documentation "The unscaled GTK pixbuf holding the image data of the button")
   (image :accessor button-view-gtk-image
	  :initarg :image
	  :type gtk:gtk-image
	  :documentation "The GTK widget representing the image of this button")
   )
  (:documentation "GTK data of one button"))


(defmethod app-view-button-text-markup ((btn button-view-gtk) &key font-size font-color font-weight)
  (let ((label-widget (button-view-gtk-label btn)))
    (setf
     (gtk:gtk-label-label label-widget)
     (app-view-button-gtk-update-text-markup (gtk:gtk-label-label label-widget) font-size font-color font-weight))))

(defmethod app-view-button-image-size ((btn button-view-gtk) &key width height)
  (let* ((pixbuf-scaled (gdk-pixbuf:gdk-pixbuf-scale-simple
			 (button-view-gtk-image-pixbuf btn)
			 width
			 height
			 :bilinear)))
    (setf (gtk:gtk-image-pixbuf (button-view-gtk-image-pixbuf btn)) pixbuf-scaled)))

(defun app-view-button-gtk-update-text-markup (old-text new-font-size new-font-color new-font-weight)
  (let ((plain-text (nth-value 0 (ppcre:regex-replace "<span[^>]+>(.*)</span>" old-text "\\1")))
	(fmt-str (format nil "<span ~A~A~A>~~A</span>"
			 (if new-font-size "size=\"~A\"" "~A")
			 (if new-font-color "foreground=\"~A\"" "~A")
			 (if new-font-weight "weight=\"~A\"" "~A"))))
    (format nil fmt-str (or new-font-size "") (or new-font-color "") (or new-font-weight "") plain-text)))

(defmethod app-view-button-opacity ((btn button-view-gtk) opacity)
  (setf (gtk:gtk-widget-opacity (button-view-gtk-widget btn)) opacity)
  (setf (gtk:gtk-widget-opacity (button-view-gtk-label btn)) opacity))
