;;;; mearo.lisp

(in-package #:mearo)

(opts:define-opts
  (:name :config-file
   :description "the configuration file to be used"
   :short #\c
   :long "config-file"
   :arg-parser #'identity
   :meta-var "FILE")
  (:name :config-string
   :description "the configuration to be used"
   :short #\C
   :long "config-string"
   :arg-parser #'identity
   :meta-var "STRING")
  (:name :version
   :description "output version information and exit"
   :short #\v
   :long "version")
  (:name :help
   :description "display this help and exit"
   :short #\h
   :long "help")
  )

(defun print-usage (stream)
  (format stream "~&Usage: ~A [OPTIONS]...~&" (uiop:argv0))
  (format stream "Create a simple button grid menu.~&")
  (format stream "~%")
  (if (static-conf)
      (format stream "With no config file given, the compile time configuration will be used.~&")
      (format stream "With no config file given, the default config file will be used.~&"))
  (format stream "~%")
  (opts:describe
   :suffix nil
   :usage-of nil
   :args nil
   :stream stream))

(defparameter *version* "0.1.0")

(defparameter *model* nil)
(defparameter *view* nil)

(defclass model ()
  ((opacity-selected :accessor model-opacity-selected
		     :initarg :opacity-seleted
		     :documentation "Opacity of selected buttons")
   (opacity-unselected :accessor model-opacity-unselected
		       :initarg :opacity-unseleted
		       :documentation "Opacity of unselected buttons")
   (num-rows :accessor model-num-rows
	     :initarg :num-rows
	     :documentation "Number of rows")
   (max-col :accessor model-max-col
	    :initarg :max-col
	    :documentation "Maximum number of cols")
   (num-cols-per-row :accessor model-num-cols-per-row
		     :initarg :num-cols-per-row
		     :documentation "Number of cols in each row")
   (selected-button :accessor model-selected-button
		    :initarg :selected-button
		    :documentation "'(row col) of the currently selected button, or nil")
   (action-matrix :accessor model-action-matrix
		  :initarg :action-matrix
		  :documentation "Matrix holding all the button actions")
   )
  (:default-initargs
   :opacity-seleted 1.0
   :opacity-unseleted 0.3
   :num-rows 0
   :max-col 0
   :num-cols-per-row (make-array '(0))
   :selected-button nil
   :action-matrix (make-array '(0 0))
   )
  (:documentation "Model holding all the data"))

(defmethod model-add-action ((m model) row col action)
  (when (> row (model-num-rows m))
    (setf (model-num-rows m) row))
  (when (> col (model-max-col m))
    (setf (model-max-col m) col))
  (when (or (>= row (array-dimension (model-action-matrix m) 0))
	    (>= col (array-dimension (model-action-matrix m) 1)))
    (setf (model-action-matrix m)
	  (adjust-array (model-action-matrix m)
			(list (+ 1 (model-num-rows m)) (+ 1 (model-max-col m)))
			:initial-element nil)))
  (when (>= row (array-dimension (model-num-cols-per-row m) 0))
    (setf (model-num-cols-per-row m)
	  (adjust-array (model-num-cols-per-row m) (list (+ 1 row)))))
  (when (> col (aref (model-num-cols-per-row m) row))
    (setf (aref (model-num-cols-per-row m) row) col))
  (setf (aref (model-action-matrix m) row col) action))

(defmethod model-action ((m model) row col)
  (if (or (> row (model-num-rows m))
	  (> col (aref (model-num-cols-per-row m) row)))
      nil
      (aref (model-action-matrix m) row col)))

(defmethod model-move-selection ((m model) direction)
  (if (not (model-selected-button m))
      (setf (model-selected-button m) (list 0 0))
      (let* ((cur-row (first (model-selected-button m)))
	     (cur-col (second (model-selected-button m)))
	     (next-pos
	       (cond
		 ((eq :right direction)
		  (if (<= (+ 1 cur-col) (aref (model-num-cols-per-row m) cur-row))
		      (list cur-row (+ 1 cur-col))
		      (if (<= (+ 1 cur-row) (model-num-rows m))
			  (list (+ 1 cur-row) 0)
			  (list cur-row cur-col))))
		 ((eq :left direction)
		  (if (> cur-col 0)
		      (list cur-row (- cur-col 1))
		      (if (and (> cur-row 0)
			       (<= (- cur-row 1) (model-num-rows m)))
			  (list (- cur-row 1) (aref (model-num-cols-per-row m) (- cur-row 1)))
			  (list cur-row cur-col))))
		 ((eq :up direction)
		  (if (> cur-row 0)
		      (list (- cur-row 1)
			    (min cur-col (aref (model-num-cols-per-row m) (- cur-row 1))))
		      (list cur-row cur-col)))
		 ((eq :down direction)
		  (if (<= (+ 1 cur-row) (model-num-rows m))
		      (list (+ 1 cur-row)
			    (min cur-col (aref (model-num-cols-per-row m) (+ 1 cur-row))))
		      (list cur-row cur-col)))
		 (t (list cur-row cur-col)))))
	(setf (model-selected-button m) next-pos))))

(defmethod model-selected-row ((m model))
  (first (model-selected-button m)))

(defmethod model-selected-col ((m model))
  (second (model-selected-button m)))

(defun deselect-all-buttons ()
  (loop for row-num from 0 to (model-num-rows *model*)
	do (loop for col-num from 0 to (aref (model-num-cols-per-row *model*) row-num)
		 do (app-view-button-opacity (app-view-button *view* row-num col-num) (model-opacity-unselected *model*)))))

(defun on-key-pressed (widget event)
  (declare (ignore widget))
  (let ((key-name (gdk:gdk-keyval-name (gdk:gdk-event-key-keyval event))))
    (cond
      ((string= "Escape" key-name) (app-view-quit))
      ((or (string= "Right" key-name) (string= "Tab" key-name))
       (model-move-selection *model* :right)
       (deselect-all-buttons)
       (app-view-button-opacity (app-view-button *view* (model-selected-row *model*) (model-selected-col *model*))
				(model-opacity-selected *model*)))
      ((or (string= "Left" key-name) (string= "ISO_Left_Tab" key-name))
       (model-move-selection *model* :left)
       (deselect-all-buttons)
       (app-view-button-opacity (app-view-button *view* (model-selected-row *model*) (model-selected-col *model*))
				(model-opacity-selected *model*)))
      ((string= "Down" key-name)
       (model-move-selection *model* :down)
       (deselect-all-buttons)
       (app-view-button-opacity (app-view-button *view* (model-selected-row *model*) (model-selected-col *model*))
				(model-opacity-selected *model*)))
      ((string= "Up" key-name)
       (model-move-selection *model* :up)
       (deselect-all-buttons)
       (app-view-button-opacity (app-view-button *view* (model-selected-row *model*) (model-selected-col *model*))
				(model-opacity-selected *model*)))
      ((or (string= "Return" key-name) (string= "space" key-name))
       (when (and (<= 0 (model-selected-row *model*)) (<= 0 (model-selected-col *model*)))
	 (on-button-pressed (model-selected-row *model*) (model-selected-col *model*))))))
  t)

(defun on-button-pressed (row col)
  (let ((action (model-action *model* row col)))
    (when action
      (uiop:run-program action))
    (app-view-quit)))

(defun initialize-gtk-ui (configuration)
  (validate-configuration configuration)
  (setf *model* (make-instance 'model))
  (setf (model-opacity-selected *model*) 1.0)
  (setf (model-opacity-unselected *model*)
	(get-conf-property configuration :dim-opacity "0.3"))
  (gtk:within-main-loop
    (setf *view* (make-app-view-gtk
		  (get-conf-property configuration :title)
		  (get-conf-property configuration :width "750")
		  (get-conf-property configuration :height "225")))
    (loop for row in (get-conf-rows configuration)
	  for row-num from 0
	  do (loop for button-conf in row
		   for col-num from 0
		   do (let ((button-action (get-button-conf-property button-conf :action))
			    (button-width (button-conf-property-p button-conf :width))
			    (button-height (button-conf-property-p button-conf :height))
			    (btn (app-view-add-button *view* row-num col-num (get-button-conf-property button-conf :image) (get-button-conf-property button-conf :label))))
			(app-view-button-text-markup btn
						     :font-size (get-conf-property configuration :font-size nil)
						     :font-color (get-conf-property configuration :font-color nil)
						     :font-weight (get-conf-property configuration :font-weight nil))
			(when (and button-width button-height)
			  (app-view-button-image-size btn button-width button-height))
			(model-add-action *model* row-num col-num button-action))))
    (when (get-conf-property configuration :background-color)
      (app-view-window-background *view* (get-conf-property configuration :background-color)))
    (app-view-window-add-keyboard-handler *view* #'on-key-pressed)
    (app-view-window-add-button-pressed-handler *view* #'on-button-pressed)
    (app-view-show *view*))
  (gtk:join-gtk-main)
  (uiop:quit))

(defun main (args)
  (multiple-value-bind (options free-args)
      (opts:get-opts args)
    (declare (ignore free-args))
    (when (getf options :help)
      (print-usage uiop:*stdout*)
      (uiop:quit 0))
    (when (getf options :version)
      (format uiop:*stdout* "~&~A~&" *version*)
      (uiop:quit 0))
    (let ((configuration (cond
			   ((getf options :config-file)
			    (load-conf-from-file (getf options :config-file)))
			   ((getf options :config-string)
			    (load-conf-from-string (getf options :config-string)))
			   ((static-conf) (static-conf))
			   (t (load-conf-from-file (default-config-file-path))))))
      (initialize-gtk-ui configuration)
      (uiop:quit 0))))

(defun plain-main ()
  (handler-case (main uiop:*command-line-arguments*)
    (conf-parse-error (e)
      (format uiop:*stderr* "~&~A: failed to parse configuration: ~A~&" (uiop:argv0) (conf-parse-error-message e))
      (uiop:quit 1))
    (conf-error (e)
      (format uiop:*stderr* "~&~A: ~A~&" (uiop:argv0) e)
      (uiop:quit 1))
    (conf-grid-error (e)
      (format uiop:*stderr* "~&~A: ~A~&" (uiop:argv0) e)
      (uiop:quit 1))))
