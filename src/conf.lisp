;;;; conf.lisp

(in-package #:mearo)

(defun default-config-file-path ()
  (uiop:xdg-config-home "mearo" "config.lisp-expr"))

(define-condition conf-parse-error (error)
  ((message
    :initarg :message
    :reader conf-parse-error-message
    :initform nil
    :documentation "Text message describing the parse error"
    )))

(define-condition conf-error (error)
  ((message
    :initarg :message
    :accessor conf-error-message
    :initform nil
    :documentation "Text message describing the error")
   (key
    :initarg :key
    :accessor conf-error-key
    :initform nil
    :documentation "Key for which the error has been signaled")))

(defmethod print-object ((obj conf-error) stream)
  (if (conf-error-key obj)
      (format stream "configuration: ~a: ~a"
	      (conf-error-key obj)
	      (conf-error-message obj))
      (format stream "configuration: ~a"
	      (conf-error-message obj))))


(defun conf-error (message &key key)
  (error 'conf-error :message message :key key))

(define-condition conf-grid-error (error)
  ((message
    :initarg :message
    :accessor conf-grid-error-message
    :initform nil
    :documentation "Text message describing the error")
   (key
    :initarg :key
    :accessor conf-grid-error-key
    :initform nil
    :documentation "Key for which the error has been signaled")
   (row
    :initarg :row
    :accessor conf-grid-error-row
    :initform nil
    :documentation "Row of the grid in which the conf the error occurred")
   (col
    :initarg :col
    :accessor conf-grid-error-col
    :initform nil
    :documentation "Column of the grid in which the conf the error occurred")))

(defmethod print-object ((obj conf-grid-error) stream)
  (let ((key-info (if (conf-grid-error-key obj)
		      (format nil "~a: " (conf-grid-error-key obj))
		      ""))
	(grid-info (cond
		     ((and (conf-grid-error-row obj) (conf-grid-error-col obj))
		      (format nil "[~a ~a]" (conf-grid-error-row obj) (conf-grid-error-col obj)))
		     ((conf-grid-error-row obj)
		      (format nil "[~a]" (conf-grid-error-row obj)))
		     ((conf-grid-error-col obj)
		      (format nil "[~a]" (conf-grid-error-col obj)))
		     (t ""))))
    (format stream "configuration button grid~a: ~a~a" grid-info key-info (conf-grid-error-message obj))))

(defun conf-grid-error (message &key key row col)
  (error 'conf-grid-error :message message :key key :row row :col col))

(defun validate-required (value)
  (if (not value) "not specified" nil))

(defun validate-string (value)
  (if (and value (not (stringp value))) "not a string" nil))

(defun validate-non-empty (value)
  (if (and value (= 0 (length value))) "value must not be empty"  nil))

(defun validate-integer (value)
  (if (and value (not (integerp value))) "not an integer value" nil))

(defun validate-float (value)
  (if (and value (not (floatp value))) "not a floating point value" nil))

(defun validate-matches (regex)
  (lambda (value)
    (if (and value (not (ppcre:scan regex value))) "invalid format" nil)))

(defun validate-strictly-positive (value)
  (if (and value (>= 0 value)) "must be strictly positive" nil))

(defun validate-between (minimum maximum)
  (lambda (value)
    (if (and value (or (> minimum value) (> value maximum)))
	(format nil "must be between [~a ~a]" minimum maximum)
	nil)))

(defparameter *conf-desc*
  (list
   (list
    :title (list #'validate-string #'validate-non-empty)
    :width (list #'validate-integer #'validate-strictly-positive)
    :height (list #'validate-integer #'validate-strictly-positive)
    :background-color (list #'validate-string #'validate-non-empty (validate-matches "^#[0-9a-fA-F]{1,6}$"))
    :font-size (list #'validate-string #'validate-non-empty)
    :font-color (list #'validate-string #'validate-non-empty (validate-matches "^#[0-9a-fA-F]{1,6}$"))
    :font-weight (list #'validate-string #'validate-non-empty)
    :dim-opacity (list #'validate-float (validate-between 0.0 1.0))
    )
   (list
    :label (list #'validate-required #'validate-string #'validate-non-empty)
    :action (list #'validate-string)
    :image (list #'validate-required #'validate-string #'validate-non-empty)
    :width (list #'validate-integer #'validate-strictly-positive)
    :height (list #'validate-integer #'validate-strictly-positive)
    )
   )
  )

;;;; for the font markup validation, see https://docs.gtk.org/Pango/pango_markup.html
(defun validate-global-configuration (conf)
  (cond
    ((not (evenp (length conf)))
     (conf-error "invalid property list (odd list length)"))
    ((not (loop :for keys-idx :from 0 :below (length conf) :by 2
		:always (symbolp (nth keys-idx conf))))
     (conf-error "invalid property list"))
    (t
     (loop :for place :in conf :by #'cddr
	   :always
	   (if (not (getf (first *conf-desc*) place))
	       (conf-error "unknown key" :key place)
	       (loop :for validator :in (getf (first *conf-desc*) place)
		     :always
		     (let ((validation-result (funcall validator (getf conf place))))
		       (if validation-result
			   (conf-error validation-result :key place)
			   t))))))))

(defun validate-button-row-configuration (conf)
  (cond
    ((not (listp conf))
     (conf-grid-error "not a list"))
    ((= 0 (length conf))
     (conf-grid-error "empty"))
    (t
     (loop :for button-conf :in conf
	   :for col :from 1
	   :always
	   (handler-bind
	       ((conf-grid-error #'(lambda (e) (setf (conf-grid-error-col e) col))))
	     (validate-button-configuration button-conf))))))

(defun validate-button-configuration (conf)
  (cond
    ((not (listp conf))
     (conf-grid-error "not a list"))
    ((not (evenp (length conf)))
     (conf-grid-error "invalid property list (odd list length)"))
    ((= 0 (length conf))
     (conf-grid-error "empty"))
    ((not (loop :for keys-idx :from 0 :below (length conf) :by 2
		:always (symbolp (nth keys-idx conf))))
     (conf-grid-error "invalid property list"))
    (t
     (loop :for place :in conf :by #'cddr
	   :always
	   (if (not (getf (second *conf-desc*) place))
	       (conf-grid-error "unknown key" :key place)
	       (loop :for validator :in (getf (second *conf-desc*) place)
		     :always
		     (let ((validation-result (funcall validator (getf conf place))))
		       (if validation-result
			   (conf-grid-error validation-result :key place)
			   t))))))))

(defun validate-configuration (conf)
  (restart-case
      (cond
	((not (listp conf))
	 (conf-error "not a list"))
	((= 0 (length conf))
	 (conf-error "empty"))
	((= 1 (length conf))
	 (conf-error "no button grid information"))
	(t
	 (and
	  (validate-global-configuration (first conf))
	  (loop :for button-row-conf :in (cdr conf)
		:for row :from 1
		:always
		(handler-bind
		    ((conf-grid-error #'(lambda (e) (setf (conf-grid-error-row e) row))))
		  (validate-button-row-configuration button-row-conf))))))
    (return-nil () nil)))

(defun load-conf-from-file (filename)
  (handler-case (with-open-file (in filename :if-does-not-exist nil)
		  (if (not in)
		      (error 'conf-parse-error :message "File not found")
		      (with-standard-io-syntax
			(read in))))
    (end-of-file ()
      (error 'conf-parse-error :message "Unexpected end of file"))))

(defun load-conf-from-string (conf-string)
  (handler-case (with-standard-io-syntax
		  (nth-value 0 (read-from-string conf-string)))
    (end-of-file ()
      (error 'conf-parse-error :message "Unexpected end of file"))))

(defun get-conf-property (conf prop &optional (default nil))
  (or (getf (first conf) prop) default))

(defun get-conf-number-of-rows (conf)
  (length (cdr conf)))

(defun get-conf-rows (conf)
  (cdr conf))

(defun get-conf-row (conf row)
  (nth row (cdr conf)))

(defun get-row-conf-number-of-cols (row-conf)
  (length row-conf))

(defun get-row-conf-button (row-conf col)
  (nth col row-conf))

(defun get-button-conf-property (button-conf prop &optional (default nil))
  (or (getf button-conf prop) default))

(defun button-conf-property-p (button-conf prop)
  (if (getf button-conf prop) t nil))
