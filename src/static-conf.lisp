;;;; static-conf.lisp

(in-package #:mearo)

#+nil
(defparameter *static-conf*
  '((:title "mearo"
     :width 550
     :height 250
     :background-color "#282c34"
     :font-size "12pt"
     :font-color "#8e95a5"
     :font-weight "bold"
     :dim-opacity 0.3)
    ((:label "Reboot"
      :action "systemctl reboot"
      :image "/usr/share/icons/Papirus/64x64/apps/system-reboot.svg")
     (:label "Shutdown"
      :action "systemctl poweroff"
      :image "/usr/share/icons/Papirus/64x64/apps/system-shutdown.svg")
     (:label "Suspend"
      :action "systemctl suspend"
      :image "/usr/share/icons/Papirus/64x64/apps/system-suspend.svg")
     (:label "Hibernate"
      :action "systemctl hibernate"
      :image "/usr/share/icons/Papirus/64x64/apps/system-hibernate.svg"))
    ((:label "Cancel"
      :action ""
      :image "/usr/share/icons/Papirus/64x64/apps/gnome-panel-force-quit.svg")
     (:label "Logout"
      :action "i3-msg 'exit'"
      :image "/usr/share/icons/Papirus/64x64/apps/gnome-logout.svg")
     (:label "Lock"
      :action "i3lock"
      :image "/usr/share/icons/Papirus/64x64/apps/system-lock-screen.svg"))
    ))

(defun static-conf ()
  (multiple-value-bind (symbol status)
      (uiop:find-symbol* "*STATIC-CONF*" :mearo nil)
    (declare (ignore status))
    (uiop:eval-thunk symbol)))
